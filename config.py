KMpH = 0.277778
LANE_WIDTH = 3

class Config:
    def __init__(self):
        self.MIN_SPAWN_DIST = 30
        self.LANES_COUNT = 5
        self.SPAWN_TIME_MEAN = 0  # seconds
        self.SPAWN_TIME_DISPERSION = 0.1
        self.AGGRESSION_MEAN = 0.5  # MODIFIABLE BY GUI
        self.AGGRESSION_DISPERSION = 0.3  # MODIFIABLE BY GUI
        self.HIGHWAY_LENGTH = 1000 # meters
        self.FILL_NEIGHBOURS_TICK_RATE = 1
