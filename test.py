import unittest

from car import Car, CarModel
from config import KMpH
from highway import Highway
from util import Point, LinkedList


class Test(unittest.TestCase):

    def test_linked_list(self):
        t = LinkedList()
        t.add(1)
        t.add(4)
        t.add(9)
        self.assertEqual([x for x in t], [9, 4, 1])
        t.pop()
        t.pop()
        self.assertEqual([x for x in t], [9])
        t.add(15)
        self.assertEqual([x for x in t], [15, 9])
        t.pop()
        t.pop()
        t.pop()
        self.assertEqual([x for x in t], [])

    def test_highway_add(self):
        regular_car = CarModel("Regular", 1.72, 4.4, 4, -3)  # Solaris
        h = Highway(2)
        h.add(regular_car, 0.5)
        self.assertEqual(len([x.lane for x in h]), 1)
        b = h.add(regular_car, 0.5)
        self.assertEqual({x.lane for x in h}, {0, 1})
        self.assertEqual(b, True)
        b = h.add(regular_car, 0.5)
        self.assertEqual(b, False)

    def test_linked_list_sort(self):
        a = [1, 4, 3, 7, 5]
        L = LinkedList()
        for x in a:
            L.add(x)
        L.sort()
        a.sort()
        self.assertEqual([x for x in L], a)
        a = [1, -4, 7, 3, 5, 8]
        L = LinkedList()
        for x in a:
            L.add(x)
        L.sort()
        a.sort()
        self.assertEqual([x for x in L], a)

    def test_linked_list_neighbours(self):
        L = LinkedList()
        inp = [5, 4, 3, 2, 1]
        for x in inp:
            L.add(x)
        y = L.get_all_neighbours(2, 2)
        ans = [
            {2, 3},
            {1, 3, 4},
            {1, 2, 4, 5},
            {2, 3, 5},
            {3, 4}
        ]
        self.assertEqual([set(x) for x in y], ans)


    def test_add_car_to_fov(self):
        l = [1,2,3]
        print(l[4:])