from random import gauss, randint
from typing import Tuple

from car import CarModel
from config import Config, KMpH
from highway import Highway


class World:
    def __init__(self, car_models: Tuple[CarModel, ...], config: Config):
        self.highway = Highway(config)
        self.config = config
        self.car_models = car_models
        self.stop = False
        self.cars = []
        self.cars_finished = 0
        self.clock, tick_count = 0.0, 0
        self.spawn_timer = 0

    def _refresh_stats(self, tick_time):
        for c in self.cars:
            if c['alive'] and c['obj'].center.x > self.config.HIGHWAY_LENGTH:
                c['alive'] = False
            if c['alive']:
                if not c['was_broken'] and c['obj'].is_broken:
                    c['was_broken'] = True
                c['alive_t'] += tick_time
                c['avg_v'] = (c['obj'].center.x - Highway.START_X) / c['alive_t']

    def collect_stats(self):
        ans = {}
        len_cars = len(self.cars)
        on_track = [c['avg_v'] for c in self.cars if c['alive']]
        if len_cars > 0:
            ans['V_mean now'] = '{0:.2f} KM/h'.format((1/KMpH) * sum(on_track) / len(on_track))
            ans['V_mean overall'] = '{0:.2f} KM/h'.format((1 / KMpH) * sum([c['avg_v'] for c in self.cars]) / len_cars)
        else:
            ans['V_mean now'] = '{0:.2f} KM/h'.format(0.0)
            ans['V_mean overall'] = '{0:.2f} KM/h'.format(0.0)
        ans['Cars on highway'] = len(on_track)
        ans['Total accidents'] = len([c for c in self.cars if c['was_broken']])
        return ans

    def tick(self, tick_time):
        """
        main method
        """
        self.clock += tick_time
        if self.spawn_timer <= 0:
            car = self.highway.add(
                    car_model=self.car_models[randint(0, len(self.car_models) - 1)],
                    aggression_rate=gauss(self.config.AGGRESSION_MEAN, self.config.AGGRESSION_DISPERSION),
            )
            if car is not None:
                self.cars.append(
                    {'obj': car, 'avg_v': car.velocity, 'alive_t': 0.0, 'alive': True, 'was_broken': False}
                )
            self.spawn_timer = gauss(self.config.SPAWN_TIME_MEAN, self.config.SPAWN_TIME_DISPERSION)
        else:
            self.spawn_timer -= tick_time
        self._refresh_stats(tick_time)
        self.highway.tick(tick_time)
        self.cars_finished += self.highway.pop(
            condition=lambda car: car.center.x > self.config.HIGHWAY_LENGTH)
