from typing import List, Any, Optional


class Point:
    def __init__(self, x: float, y: float):
        self.x, self.y = x, y

    def __iadd__(self, other):
        self.x = self.x + other.x
        self.y = self.y + other.y
        return self
