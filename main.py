import pygame
import os
from tkinter import *
from tkinter import ttk

from PIL import Image, ImageTk

from datetime import datetime, timedelta
from config import Config, KMpH, LANE_WIDTH
from world import World
from car import CarModel, Car

sport_car = CarModel("Sport", 1.85, 4.468, 5, -10)  # BMW 2
regular_car = CarModel("Regular", 1.72, 4.4, 4, -8)  # Solaris
old_car = CarModel("Old", 1.8, 4.5, 2, -8)  # Zhiga
truck = CarModel("Truck", 2.5, 11, 1, -5)  # Truck
car_models = (sport_car, regular_car, old_car, truck)





class GUI:
    MAX_LANES = 8

    def __init__(self):
        self.car_models = {x.name: (x.width, x.length) for x in car_models}
        self.config = Config()
        self.root = Tk()
        pygame.font.init()
        self.root.minsize(1000, 700)
        self._init_global_params()
        self._init_params()

        self.left_panel = Frame(self.root, width=300, height=440, bg='grey')
        self.center_panel = Frame(self.root, width=900, height=440, bg='grey')

        self.left_panel.pack(side='left', fill='y')
        self.center_panel.pack(side='right', fill='both', expand=True)

        self._draw_left_panel()
        self._draw_center_panel()
        self._display_bg()

        self.last_tick = datetime.now()
        self._mainloop()

    def x(self, x_in_world):
        return int(-self.x_shift + x_in_world*self.meter)

    def y(self, y_in_world, zero=0):
        if zero == 0:
            zero = self.y_zero
        return int(zero + y_in_world*self.meter)

    def _mainloop(self):
        self.clock = pygame.time.Clock()
        self.ticks_count = 0
        fps_text = self.fps_font.render(f"{round(1000/self.fps)}fps", False, (0, 0, 0))
        fps = []
        self._show_stats(self.world.collect_stats())
        try:
            while 1:
                self._handle_pygame_events()
                self.ticks_count += 1

                self._display_bg()
                self._do_screen_movement()
                self.root.update()
                dt = self.clock.tick_busy_loop(self.fps)
                fps.append(dt)
                if not self.is_paused and self.is_active:
                    self.time += timedelta(seconds=self.time_speed*(dt*0.001))
                    self.time_var.set(self.__get_time())
                    if (self.time - self.last_stats_time).total_seconds() > 1:
                        fps_text = self.fps_font.render(f"{round(1000/(sum(fps)/len(fps)))}fps", False, (0, 0, 0))
                        fps = []
                        self._show_stats(self.world.collect_stats())
                        self.last_stats_time = self.time
                    self.world.tick(self.time_speed * dt * 0.001)
                self.canvas.blit(fps_text, (0, 0))
                self._render(self.world.highway)
                pygame.display.flip()
                self.root.update()
        except TclError:
            print('quit')

    def _handle_pygame_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT: sys.exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT: self._left()
                if event.key == pygame.K_RIGHT: self._right()
            if event.type == pygame.MOUSEBUTTONUP or event.type == pygame.MOUSEBUTTONDOWN:
                pos = pygame.mouse.get_pos()
                if self.y_zero < pos[1] < self.y_one:
                    pos = (pos[0] + self.x_shift)/self.meter, (pos[1] - self.y_zero)/self.meter
                    car = self.world.highway.get_at_pos(pos)
                    if car is not None:
                        car.repair_time = 60.0
                        car.is_broken = True

        keys_pressed = pygame.key.get_pressed()
        if keys_pressed[pygame.K_LEFT]: self._left()
        if keys_pressed[pygame.K_RIGHT]: self._right()

    def _display_bg(self):
        for x in range(0, self.config.HIGHWAY_LENGTH * self.meter, self.size[0]):
            self.canvas.blit(self.bg, (int(-self.x_shift + x), 0))
        for x in range(0, self.config.HIGHWAY_LENGTH, 100):
            pygame.draw.line(self.canvas, (255, 0, 0),
                             (self.x(x), self.y(-2)), (self.x(x), self.y(7, self.y_one)),
                             self.meter // 2)
            text = self.meters_font.render('{0}m'.format(x), False, (255, 0, 0))
            self.canvas.blit(text, (self.x(x + 1), self.y(2, self.y_one)))

    def _init_global_params(self):
        self.fps = 60
        self.speed_overlay, self.aggression_overlay = False, False
        self.zoom = 1.0
        self.time_speed = 1.0
        self.x_shift, self.x_shift_delta = 0, 0
        self.overlay_var = IntVar()
        self.stats_values_var, self.stats_names_var = StringVar(), StringVar()
        self.aggr_mu, self.aggr_sigma, self.rep_time = StringVar(), StringVar(), StringVar()
        self._fill_entries()

    def _init_params(self):
        self.ticks_count, self.is_paused, self.is_active = 0, False, False,
        self.time = datetime(100, 1, 1, 0, 0, 0)
        self.last_stats_time = datetime(100, 1, 1, 0, 0, 0)
        self.cars, self.marked, self.canvas_objects = {}, set(), []
        self.world = World(car_models, self.config)
        self._show_stats(self.world.collect_stats())


    def _draw_center_panel(self, event=None):
        os.environ['SDL_WINDOWID'] = str(self.center_panel.winfo_id())

        self.root.update()
        self.size = self.center_panel.winfo_width(), self.center_panel.winfo_height()
        self.canvas = pygame.display.set_mode(self.size, flags=pygame.RESIZABLE)
        self._canvas_init()
        self.canvas.blit(self.bg, self.bg.get_rect())

        self.root.bind('<Configure>', self._draw_center_panel)
        self.root.focus_force()
        self.root.bind('<Right>', self._right)
        self.root.bind('<Left>', self._left)


    def _canvas_init(self, event=None):
        self.root.update()
        self.size = self.center_panel.winfo_width(), self.center_panel.winfo_height()

        self.meter = int(self.zoom * self.size[1] / 100)

        y_center = self.size[1] // 2
        self.y_height = (self.config.LANES_COUNT * LANE_WIDTH + 2) * self.meter
        self.y_top = y_center - self.y_height // 2
        self.y_zero = self.y_top + self.meter
        self.y_one = y_center + self.y_height // 2 - self.meter
        self.bg = self._draw_bg()

        self.meters_font = pygame.font.SysFont('Courier', int(5*self.meter))
        self.overlay_font = pygame.font.SysFont('Courier', int(3*self.meter))
        self.fps_font = pygame.font.SysFont('Courier', 12)

        if self.config.HIGHWAY_LENGTH * self.meter / 2 < self.x_shift + self.size[0]:
            self.x_shift = self.config.HIGHWAY_LENGTH * self.meter / 2 - self.size[0]

        imgs = {
            'Truck': (os.path.join('textures', 'truck.png'),  truck.width, truck.length),
            'Sport': (os.path.join('textures', 'sport.png'), sport_car.width, sport_car.length),
            'Regular': (os.path.join('textures', 'SUV.png'), regular_car.width, regular_car.length),
            'Old': (os.path.join('textures', 'sedan.png'), old_car.width, old_car.length)
        }
        self.car_images = {key: pygame.transform.smoothscale(
                                    pygame.image.load(val[0]),
                                    (int(val[2] * self.meter), int(val[1]*self.meter))
                                ) for key, val in imgs.items()}

    def _draw_bg(self):
        bg = pygame.Surface(self.size)
        grass = (126, 200, 80)
        asphalt = (150, 150, 150)
        bg.fill(grass)
        bg.fill(asphalt, (0, self.y_top, self.config.HIGHWAY_LENGTH*self.meter, self.y_height))

        sep_width, sep_length = round(0.2 * self.meter), int(3 * self.meter)
        pygame.draw.line(bg, (255, 255, 255), (0, self.y_zero), (self.size[0], self.y_zero), sep_width)
        pygame.draw.line(bg, (255, 255, 255), (0, self.y_one), (self.size[0], self.y_one), sep_width)
        seps = [
            (
                    (x*sep_length*2, self.y_zero - sep_width + i*LANE_WIDTH*self.meter),
                    (sep_length + x*sep_length*2, self.y_zero - sep_width + i*LANE_WIDTH*self.meter)
            )
            for x in range(int(self.config.HIGHWAY_LENGTH // 3)) for i in range(1, self.config.LANES_COUNT)
        ]
        for sep_start, sep_end in seps:
            pygame.draw.line(bg, (255, 255, 255), sep_start, sep_end, sep_width)
        pygame.draw.line(bg, (255, 255, 255), (0, self.y_zero), (self.size[0], self.y_zero), sep_width)

        return bg

    def _draw_left_panel(self):
        self._init_widgets()
        self._pack_widgets()

    def _init_widgets(self):
        width = 170
        self.settings_panel = Frame(self.left_panel, width=width, height=100, bg='white')
        self.settings_label = Label(self.settings_panel, text='Settings', font='Helvetica 18', bg='white')
        self.lanes_scale_label = Label(self.settings_panel, text='Lanes', font='Helvetica 12', bg='white')
        self.lanes_scale = Scale(self.settings_panel,
                                 command=self.__lanes_scale_move, bg='white', activebackground='white',
                                 orient=HORIZONTAL, highlightbackground='white', highlightcolor='white',
                                 showvalue=0, bd=0, length=width, font='Helvetica 8',
                                 from_=1, to=8, resolution=1, tickinterval=1)
        self.lanes_scale.set(5)
        self.aggr_mu_label = Label(self.settings_panel, text='Aggression mu', font='Helvetica 10', justify=LEFT,
                                   bg='white')
        self.aggr_mu_entry = Entry(self.settings_panel, width=4, textvariable=self.aggr_mu, font='Helvetica 10',
                                   bg='white')
        self.aggr_mu_entry.bind('<FocusOut>', self._change_config)
        self.aggr_mu_entry.bind('<Return>', lambda e: self.root.focus())
        self.aggr_sigma_label = Label(self.settings_panel, text='sigma', font='Helvetica 10', bg='white')
        self.aggr_sigma_entry = Entry(self.settings_panel, width=4, textvariable=self.aggr_sigma, font='Helvetica 10')
        self.aggr_sigma_entry.bind('<FocusOut>', self._change_config)
        self.aggr_sigma_entry.bind('<Return>', lambda e: self.root.focus())
        self.rep_time_label = Label(self.settings_panel, text='Repair time', font='Helvetica 10', bg='white')
        self.rep_time_entry = Entry(self.settings_panel, width=4, textvariable=self.rep_time, font='Helvetica 10')
        self.rep_time_entry.bind('<FocusOut>', self._change_config)
        self.rep_time_entry.bind('<Return>', lambda e: self.root.focus())

        self.traffic_scale_label = Label(self.settings_panel,
                                         text='Traffic intensity', font='Helvetica 12', bg='white')
        self.traffic_scale = Scale(self.settings_panel,
                                   command=self.__traffic_scale_move, showvalue=0, bd=0, length=width,
                                   bg='green', activebackground='green', orient=HORIZONTAL,
                                   highlightbackground='white', highlightcolor='white',
                                   from_=10, to=100, resolution=10)
        self.traffic_scale.set(50)

        self.playback_panel = Frame(self.left_panel, width=width, height=100, bg='white')
        self.time_var = StringVar()
        self.time_var.set(self.__get_time())
        self.time_label = Label(self.playback_panel,
                                textvariable=self.time_var, bg='white', justify=CENTER, font='Symbol 24')
        self.time_combobox = ttk.Combobox(self.playback_panel, state='readonly', width=5,
                                          values=('0.25x', '0.5x', '0.75x', '1x', ' 2x', ' 3x', ' 4x'))
        self.time_combobox.bind('<<ComboboxSelected>>', self.set_speed)
        self.time_combobox.current(3)

        self.play_img = ImageTk.PhotoImage(Image.open(r'textures/play_button.png').resize((30, 30)))
        self.pause_img = ImageTk.PhotoImage(Image.open(r'textures/pause.png').resize((30, 30)))
        self.stop_img = ImageTk.PhotoImage(Image.open(r'textures/stop_button.png').resize((30, 30)))
        self.play_button = Button(self.playback_panel, image=self.play_img,  bg='white', command=self._unpause)
        self.pause_button = Button(self.playback_panel, image=self.pause_img,  bg='white', command=self._pause)
        self.halt_button = Button(self.playback_panel, image=self.stop_img,  bg='white', command=self._stop)

        self.plus_zoom_img = ImageTk.PhotoImage(Image.open(r'textures/addzoom.png').resize((30, 30)))
        self.minus_zoom_img = ImageTk.PhotoImage(Image.open(r'textures/subzoom.png').resize((30, 30)))
        self.zoom_panel = Frame(self.left_panel, width=width, height=100, bg='white')
        self.zoom_plus_button = Button(self.zoom_panel, image=self.plus_zoom_img, command=self._add_zoom, bg='white')
        self.zoom_minus_button = Button(self.zoom_panel, image=self.minus_zoom_img, command=self._sub_zoom, bg='white')
        self.overlay_panel = Frame(self.zoom_panel, bg='white')
        self.overlay_label = Label(self.overlay_panel, text='Overlay', bg='white', justify=CENTER, font='Helvetica 12')
        self.v_radiobutton = Radiobutton(self.overlay_panel, text='Velocity', variable=self.overlay_var, value=1,
                                         justify=LEFT, anchor=W, bg='white', borderwidth=0, font='Helvetica 10')
        self.aggr_radiobutton = Radiobutton(self.overlay_panel, text='Aggression', variable=self.overlay_var, value=2,
                                         justify=LEFT, anchor=W, bg='white', borderwidth=0, font='Helvetica 10')
        self.none_radiobutton = Radiobutton(self.overlay_panel, text='None', variable=self.overlay_var, value=0,
                                         justify=LEFT, anchor=W, bg='white', borderwidth=0, font='Helvetica 10')

        self.stats_panel = Frame(self.left_panel, width=width, height=100, bg='white')
        self.stats_names = Message(self.stats_panel, bg='white', font='monospace 8',
                                   textvariable=self.stats_names_var, justify=LEFT)
        self.stats_values = Message(self.stats_panel, bg='white', font='monospace 8',
                                    textvariable=self.stats_values_var, justify=RIGHT)

    def _pack_widgets(self):
        self.settings_panel.grid(row=1, column=0, sticky=W+E, padx=2, pady=2)
        self.playback_panel.grid(row=7, column=0, sticky=S + W + E + N, padx=2, pady=2)
        self.zoom_panel.grid(row=9, column=0, sticky=S + W + E + N, padx=2, pady=2)
        self.stats_panel.grid(row=11, column=0, sticky=S + W + E + N, padx=2, pady=2)

        self.settings_label.grid(row=0, column=0,columnspan=2, sticky=S+W+E+N)
        self.lanes_scale_label.grid(row=1, column=0, columnspan=2, sticky=S+W+E+N)
        self.lanes_scale.grid(row=2, column=0, columnspan=2, sticky=S+W+E+N)
        self.traffic_scale_label.grid(row=3, column=0, columnspan=2, sticky=S+W+E+N)
        self.traffic_scale.grid(row=4, column=0, columnspan=2, sticky=S+W+E+N)
        self.aggr_mu_label.grid(row=5, column=0, sticky=E)
        self.aggr_mu_entry.grid(row=5, column=1, sticky=S+W+E+N)
        self.aggr_sigma_label.grid(row=6, column=0, sticky=E)
        self.aggr_sigma_entry.grid(row=6, column=1, sticky=S+W+E+N)
        self.rep_time_label.grid(row=7, column=0, sticky=E)
        self.rep_time_entry.grid(row=7, column=1, sticky=S+W+E+N)
        self.time_label.grid(row=0, column=0, columnspan=4, sticky=E)
        self.time_combobox.grid(row=1, column=0, padx=5)
        self.play_button.grid(row=1, column=1, pady=2, sticky=E)
        self.pause_button.grid(row=1, column=2,pady=2, sticky=E)
        self.halt_button.grid(row=1, column=3, pady=2 , sticky=E)
        self.zoom_plus_button.grid(row=0, column=1, padx=2, pady=1, sticky=N+E+S)
        self.zoom_minus_button.grid(row=1, column=1, padx=2, pady=1, sticky=N+E+S)
        self.overlay_panel.grid(row=0, column=0, rowspan=2,sticky=N+E+W+S)
        self.zoom_panel.grid_columnconfigure(0, weight=1)
        self.zoom_panel.grid_columnconfigure(1, weight=0)
        self.overlay_label.grid(row=0, column=0, padx=0, sticky=W+E)
        self.none_radiobutton.grid(row=1, column=0, padx=0, sticky=W+E)
        self.v_radiobutton.grid(row=2, column=0, padx=0, sticky=W+E)
        self.aggr_radiobutton.grid(row=3, column=0, padx=0, sticky=W+E)
        self.stats_names.pack(side=LEFT)
        self.stats_values.pack(side=RIGHT)


    def _value_to_rgba(self, val: float, min_: float, max_: float, alpha: int):
        if val < min_:
            return 255, 0, 0, alpha
        if val > max_:
            return 0, 255, 0, alpha
        half = min_ + (max_ + min_) / 2
        if val < half:
            g = 255 * (val - min_) / (half - min_)
            return 255, g, 0, alpha
        else:
            r = 255 * (1 - (val - half) / (max_ - half))
            return r, 255, 0, alpha

    def _render_signals(self):
        stops = pygame.Surface(self.size, pygame.SRCALPHA)
        red, yellow = (255, 0, 0, 150), (255, 150, 0, 150)
        for car in self.world.highway:
            bl = (self.x(car.back_left.x), self.y(car.back_left.y))
            br = (self.x(car.back_right.x), self.y(car.back_right.y))
            fl = (self.x(car.front_left.x), self.y(car.front_left.y))
            fr = (self.x(car.front_right.x), self.y(car.front_right.y))
            r = int(0.5 * self.meter)
            if car.stop_signals:
                pygame.draw.circle(stops, red, bl, r)
                pygame.draw.circle(stops, red, br, r)
            if car.left_signal:
                pygame.draw.circle(stops, yellow, bl, r)
                pygame.draw.circle(stops, yellow, fl, r)
            if car.right_signal:
                pygame.draw.circle(stops, yellow, br, r)
                pygame.draw.circle(stops, yellow, fr, r)
        return stops

    def _render(self, highway_state):
        overlay = pygame.Surface(self.size, pygame.SRCALPHA)
        for car in highway_state:
            self.canvas.blit(self.car_images[car.model.name],
                             (self.x(car.back_left.x), self.y(car.back_left.y)))

            if self.overlay_var.get() > 0:
                if self.overlay_var.get() == 1:
                    val, min_, max_, text_ = car.velocity / KMpH, 20, 80, '{0}'.format(round(car.velocity/KMpH))
                elif self.overlay_var.get() == 2:
                    val, min_, max_, text_ = car._aggression_rate, 0.1, 0.9, f'{round(100*car._aggression_rate)}'
                col = self._value_to_rgba(val, min_, max_, 100)
                pygame.draw.rect(overlay, col,
                                 (self.x(car.back_left.x - 0.5), self.y(car.back_left.y - 0.5),
                                  int((car.model.length + 1) * self.meter), int((car.model.width + 1) * self.meter)),
                                 )
                text = self.overlay_font.render(text_, False, (0,0,0))
                size = text.get_size()
                overlay.blit(text,
                             (self.x(car.center.x) - size[0]//2, self.y(car.center.y) - size[1]//2))

        self.canvas.blit(self._render_signals(), (0, 0))
        self.canvas.blit(overlay, (0, 0))

    def _do_screen_movement(self):
        if abs(self.x_shift_delta) > 0:
            TICKS_PER_CLICK = 20
            if 0 < self.x_shift + self.x_shift_delta < self.config.HIGHWAY_LENGTH * self.meter - self.size[0]:
                self.x_shift += self.x_shift_delta
                if self.ticks_count - self.start_movement >= TICKS_PER_CLICK:
                    self.x_shift_delta = 0

    def _right(self, event=None):
        self.start_movement = self.ticks_count
        self.x_shift_delta = 4

    def _left(self, event=None):
        self.start_movement = self.ticks_count
        self.x_shift_delta = -4

    def _add_zoom(self, *args):
        if self.zoom <= 2:
            self.x_shift *= 2
            self.zoom *= 2
            if self.zoom == 4:
                self.zoom_plus_button.config(state=DISABLED)
            if self.zoom > 0.5:
                self.zoom_minus_button.config(state=ACTIVE)
            self._canvas_init()
    def _sub_zoom(self, *args):

        if self.zoom >= 0.75:
            self.zoom /= 2
            self.x_shift /= 2
            if self.zoom < 4:
                self.zoom_plus_button.config(state=ACTIVE)
            if self.zoom == 0.5:
                self.zoom_minus_button.config(state=DISABLED)
            self._canvas_init()

    def _speed_overlay(self):
        self.speed_overlay = not self.speed_overlay

    def _aggr_overlay(self):
        self.aggression_overlay = not self.aggression_overlay

    def _unpause(self):
        if self.is_active:
            self.is_paused = False
        else:
            self.lanes_scale.config(state=DISABLED, takefocus=0)
            self._init_params()
            self.is_active = True

    def _pause(self):
        self.is_paused = True

    def _stop(self):
        self.lanes_scale.config(state=NORMAL, takefocus=1, bg='white')
        self.is_active = False

    def set_speed(self, event):
        new_val = self.time_combobox.current()
        values = (0.25, 0.5, 0.75, 1, 2, 3, 4)
        self.time_speed = values[new_val]

    def __lanes_scale_move(self, new):
        new_val = int(new)
        self.config.LANES_COUNT = new_val
        self._canvas_init()

    def __traffic_scale_move(self, new):
        new_val = int(new)
        cols = {10: 'green', 20: 'green', 30: 'green', 40: 'yellow', 50: 'yellow', 60: 'yellow',
                70: 'orange', 80: 'orange', 90: 'red', 100: 'red'}
        self.config.SPAWN_TIME_MEAN = (100 - new_val) / 100
        self.traffic_scale.configure(bg=cols[new_val], activebackground=cols[new_val])

    def _show_stats(self, stats):
        names = "\n".join([str(c) for c in stats.keys()])
        vals = "\n".join([str(c) for c in stats.values()])
        self.stats_names_var.set(names)
        self.stats_values_var.set(vals)

    def _fill_entries(self):
        self.aggr_mu.set(str(self.config.AGGRESSION_MEAN))
        self.aggr_sigma.set(str(self.config.AGGRESSION_DISPERSION))
        self.rep_time.set(str(Car.REPAIR_TIME))

    def _change_config(self, event=None):
        try:
            mu = float(self.aggr_mu.get())
            sigma = float(self.aggr_sigma.get())
            rep = float(self.rep_time.get())
            if 0.1 <= mu <= 0.9 and 0 <= sigma <= 0.5 and 0 <= rep:
                self.config.AGGRESSION_MEAN = float(self.aggr_mu.get())
                self.config.AGGRESSION_DISPERSION = sigma
                Car.REPAIR_TIME = float(self.rep_time.get())
            else:
                self._fill_entries()
        except ValueError:
            self._fill_entries()
            return False

    def __get_time(self):
        return self.time.strftime("%H:%M:%S:%f")[:-4]


GUI()