from random import choice
from typing import List, Tuple, Optional

from car import Car, CarModel
from util import LinkedList
from config import Config, KMpH, LANE_WIDTH


class Highway:
    START_X = -10
    FRONT_VIEW = 70
    BACK_VIEW = 50

    def __init__(self, config: Config):
        self.config = config
        self.lanes_count = config.LANES_COUNT
        self.lanes: List[List[Car]] = [[] for _ in range(self.lanes_count)]
        self.deleted: List[Car] = []
        Car.MAX_Y = LANE_WIDTH * config.LANES_COUNT
        self.tick_count = 0

    def __iter__(self):
        ans = []
        for lane in self.lanes:
            ans += lane
        return iter(ans)

    def add(self, car_model: CarModel, aggression_rate: float) -> Optional[Car]:
        """
            adds car to highway if there is enough space in any lane.
            returns True if insertion succeeded else returns False.
        """
        free_lanes = [i for i in range(self.lanes_count)]
        for i, lane in enumerate(self.lanes):
            if not(len(lane) == 0) and lane[0].back_right.x - Highway.START_X < self.config.MIN_SPAWN_DIST\
                    or (car_model.name == 'Truck' and i < self.lanes_count // 2):
                free_lanes.remove(i)
        if len(free_lanes) == 0:
            return None
        lane = choice(free_lanes)
        first_cars_v = [c.velocity for c in self.lanes[lane] if c.center.x < self.config.HIGHWAY_LENGTH / 5]
        velocity = sum(first_cars_v) / len(first_cars_v) if len(first_cars_v) > 0 else 80.0*KMpH
        car = Car(car_model, (Highway.START_X, LANE_WIDTH * (0.5 + lane)), aggression_rate, velocity)
        self.lanes[lane].insert(0, car)
        return car

    def pop(self, condition=lambda x: x) -> int:
        """
        Deletes cars from the end based on condition
        sets self.deleted with list of all deleted cars
        Returns number of cars deleted
        """
        count = 0
        for lane in self.lanes:
            for car in reversed(lane):
                if condition(car):
                    lane.pop()
                    car.clear_fov()
                    self.deleted.append(car)
                    count += 1
                else:
                    break
        return count

    def _set_neighbours(self, car: Car, lane: int, index: int) -> None:
        if lane > 0:
            for i, c in enumerate(self.lanes[lane - 1]):
                if c.center.x >= car.center.x:
                    car.cars_in_fov['frontleft'] = c if c.center.x - car.center.x < Highway.FRONT_VIEW else None
                    if i > 0 and car.center.x - self.lanes[lane - 1][i - 1].center.x < Highway.BACK_VIEW:
                        car.cars_in_fov['backleft'] = self.lanes[lane - 1][i - 1]
                    else:
                        car.cars_in_fov['backleft'] = None
                    break
        if lane < self.lanes_count - 1:
            for i, c in enumerate(self.lanes[lane + 1]):
                if c.center.x >= car.center.x:
                    car.cars_in_fov['frontright'] = c if c.center.x - car.center.x < Highway.FRONT_VIEW else None
                    if i > 0 and car.center.x - self.lanes[lane + 1][i - 1].center.x < Highway.BACK_VIEW:
                        if abs(car.center.x - self.lanes[lane + 1][i - 1].center.x > 100):
                            print(car.center.x, self.lanes[lane + 1][i - 1].center.x)
                        car.cars_in_fov['backright'] = self.lanes[lane + 1][i - 1]
                    else:
                        car.cars_in_fov['backright'] = None
                    break
        if (index < len(self.lanes[lane]) - 1
                and self.lanes[lane][index + 1].center.x - car.center.x < Highway.FRONT_VIEW):
            car.cars_in_fov['front'] = self.lanes[lane][index + 1]
        else:
            car.cars_in_fov['front'] = None

    def get_at_pos(self, pos: Tuple[float, float]) -> Optional[Car]:
        lane = int(pos[1] // LANE_WIDTH)
        for c in self.lanes[lane]:
            if c.back_left.x < pos[0] < c.front_right.x and c.back_left.y < pos[1] < c.front_right.y:
                return c
            elif pos[0] < c.front_right.x:
                return None


    def _fill_cars_in_fov(self):
        for i, lane in enumerate(self.lanes):
            for j, car in enumerate(lane):
                self._set_neighbours(car, i, j)

    def _ins_sort(self):
        for lane in self.lanes:
            for i in range(len(lane)):
                for j in range(i-1, -1, -1):
                    if lane[j].center.x > lane[i].center.x:
                        lane[j], lane[i] = lane[i], lane[j]

    def tick(self, tick_time: float) -> None:
        """
        Perform actions that must have happened in tick_time.
        """
        self._ins_sort()
        if self.tick_count % self.config.FILL_NEIGHBOURS_TICK_RATE == 0:
            self._fill_cars_in_fov()
        self.tick_count += 1
        lane_changed = []
        for i, lane in enumerate(self.lanes):
            for j, car in enumerate(lane):
                if (car.back_left.y < i*LANE_WIDTH or car.back_left.y > (i+1) * LANE_WIDTH
                        or car.back_right.y < i*LANE_WIDTH or car.back_right.y > (i+1) * LANE_WIDTH):
                    lane_changed.append((car, i, j))
                car.tick(tick_time)
        if len(lane_changed) > 0:
            self._fill_cars_in_fov()
        for car, old_lane, old_index in lane_changed:
            self.lanes[old_lane].remove(car)
            new = max(0, min(int(car.center.y // LANE_WIDTH), self.lanes_count - 1))
            self.lanes[new].insert(min(old_index, len(self.lanes[new])), car)