from math import sin, cos, radians, sqrt
from typing import Dict

from config import KMpH, LANE_WIDTH
from util import Point
from typing import Tuple

class CarModel:
    def __init__(self, name, width, length, acceleration, break_acceleration):
        self._name = name
        self._width, self._length = width, length
        self._max_acceleration, self._max_brake_acceleration = acceleration, break_acceleration

    @property
    def name(self) -> str:
        return self._name

    @property
    def max_acceleration(self) -> float:
        return self._max_acceleration

    @property
    def max_brake_acceleration(self) -> float:
        return self._max_brake_acceleration

    @property
    def width(self) -> float:
        return self._width

    @property
    def length(self) -> float:
        return self._length

class Car:
    MIN_SPEED = 60 * KMpH
    MAX_SPEED = 100 * KMpH
    FRONT_VIEW_DIST = 60
    BACK_VIEW_DIST = 30
    REACTION_TIME = 1.0
    ACCIDENT_ACCELERATION = -7
    MAX_ROTATION = 20
    BACK_SAFE_DIST = 1
    FRONT_SAFE_DIST = 1
    SPEED_TOLERANCE = 10*KMpH
    REPAIR_TIME = 60.0 # MODIFIABLE BY GUI
    MAX_Y = 5 * LANE_WIDTH

    def __init__(self, model: CarModel, start_pos: Tuple[float, float], aggression_rate: float, velocity: float):
        self.model = model

        self.center = Point(*start_pos)
        self.back_right = Point(self.center.x - model.length / 2, self.center.y + model.width / 2)
        self.back_left = Point(self.center.x - model.length / 2, self.center.y - model.width / 2)
        self.front_right = Point(self.center.x + model.length / 2, self.center.y + model.width / 2)
        self.front_left = Point(self.center.x + model.length / 2, self.center.y - model.width / 2)

        self._aggression_rate = min(0.9, max(aggression_rate, 0.1))
        self._desired_velocity = (Car.MIN_SPEED + aggression_rate * (Car.MAX_SPEED - Car.MIN_SPEED))
        self.velocity = velocity
        self._acceleration = 0
        self._rotation = 0
        self._reaction_delay = 0
        self.is_broken = False
        self.want_left, self.want_right = False, False
        self.left_signal, self.right_signal, self.stop_signals = False, False, False
        self._turn_signal_time, self._desired_y, self._timer = 60.0, self.center.y, 0.0
        self._action = None
        self.cars_in_fov = {
            pos: None
            for pos in ['front', 'back', 'frontleft', 'frontright', 'backleft', 'backright']
        }

    def _move(self, x: float, y: float) -> None:
        vect = Point(x, y)
        self.center += vect
        self.back_left += vect
        self.back_right += vect
        self.front_left += vect
        self.front_right += vect

    def clear_fov(self):
        self.cars_in_fov = {
            pos: None
            for pos in ['front', 'back', 'frontleft', 'frontright', 'backleft', 'backright']
        }

    def tick(self, tick_time: float) -> None:
        """
        Perform changes that must have happened in tick_time e.g. change position, velocity,
        react to external conditions
        """
        dist_delta = tick_time * self.velocity + 0.5 * self._acceleration * tick_time * tick_time
        y_delta = dist_delta * sin(radians(self._rotation))
        x_delta = dist_delta * cos(radians(self._rotation))
        self._move(x_delta, y_delta)
        velocity_delta = self._acceleration * tick_time
        self.velocity += velocity_delta
        self.velocity = max(self.velocity, 0)
        self._reaction_delay -= tick_time
        if self._acceleration < -0.1:
            self.stop_signals = True
        else:
            self.stop_signals = False
        if not self.is_broken:
            self._check_accidents()
        if self.want_left:
            self._turn_signal_time += tick_time
            self.left_signal = True if (round(1000 * self._turn_signal_time) % 1000) // 500 == 0 else False
        if self.want_right:
            self._turn_signal_time += tick_time
            self.right_signal = True if (round(1000 * self._turn_signal_time) % 1000) // 500 == 0 else False
        self.make_driving_decision()

    def _intersects(self, other) -> bool:
        if other is None:
            return False
        if (self.back_left.x > other.front_right.x or self.front_right.x < other.back_left.x
                or self.back_left.y > other.front_right.y or self.front_right.y < other.back_left.y):
            return False
        else:
            return True

    def _crush_into(self, other):
        self.velocity /= 2
        other.velocity = self.velocity
        other.is_broken = True
        self.is_broken = True
        print(f'crushed {self.model.name} into {other.model.name}')

    def _check_accidents(self):
        if self._intersects(self.cars_in_fov['front']):
            self._crush_into(self.cars_in_fov['front'])
        if self._intersects(self.cars_in_fov['frontleft']):
            self._crush_into(self.cars_in_fov['frontleft'])
        if self._intersects(self.cars_in_fov['frontright']):
            self._crush_into(self.cars_in_fov['frontright'])

    def _set_acceleration_for_time(self, a_power, t, breaking=True):
        if breaking:
            self._acceleration = a_power * self.model.max_brake_acceleration
        else:
            if self.velocity < self._desired_velocity:
                self._acceleration = a_power * self.model.max_acceleration
            else:
                self._acceleration = 0
        self._reaction_delay = t


    def _check_lane_change(self, where: str):
        f = self.cars_in_fov['front']
        if (where == 'left' and self.center.y < LANE_WIDTH
                or where == 'right' and self.center.y > Car.MAX_Y - LANE_WIDTH):
            return False
        diff = Car.SPEED_TOLERANCE * (1.5 - self._aggression_rate)
        if self._desired_velocity - f.velocity > diff and self.center.x > 50.0 and f is not None:
            fl = self.cars_in_fov['front' + where]
            bl = self.cars_in_fov['back' + where]
            back_dist = Car.BACK_SAFE_DIST * (1.5 - self._aggression_rate) * (bl.velocity if bl is not None else 20)
            front_dist = Car.FRONT_SAFE_DIST * (1.5 - self._aggression_rate) * self.velocity
            front_free = fl is None or (abs(fl.back_left.x - self.front_left.x) > front_dist and fl.velocity > f.velocity)
            back_free = bl is None or (abs(self.back_left.x - bl.front_right.x) > back_dist)
            side_is_faster = fl is None or fl.velocity > f.velocity + 10*self._aggression_rate*KMpH
            not_stopping_back = bl is None or f.velocity > bl.velocity + 10*self._aggression_rate*KMpH
            if front_free and back_free and side_is_faster and not_stopping_back:
                if where == 'left':
                    self._desired_y = self.center.y - LANE_WIDTH
                    self.want_left, self._turn_signal_time = True, 0.0
                if where == 'right':
                    self._desired_y = self.center.y + LANE_WIDTH
                    self.want_right, self._turn_signal_time = True, 0.0
                return True
        self.want_left, self.want_right = False, False
        return False

    def _to_desired_velocity(self):
        if self.velocity > self._desired_velocity:
            self._acceleration = 0.1 * self.model.max_brake_acceleration
        else:
            self._acceleration = 0.4 * self.model.max_acceleration
        self._reaction_delay = 1

    def _set_reaction(self) -> None:
        """
        react to road situation
        all the coefficients in this method were established by experiments
        """
        def safe_dist(v_delta, aggr, max_brake):
            return (-1.0*aggr + 1.5)*(1 + 0.1*v_delta)*(1 + (-20/max_brake))

        if self.cars_in_fov['front'] is None:
            self._to_desired_velocity()
        else:
            self._rotation = 0
            if self.want_right or self.want_left:
                if self.center.y > self._desired_y + 0.1:
                    self._rotation = -self._aggression_rate * Car.MAX_ROTATION
                    self._set_acceleration_for_time(1.0, 0.0, False)
                elif self.center.y < self._desired_y - 0.1:
                    self._rotation = self._aggression_rate * Car.MAX_ROTATION
                    self._set_acceleration_for_time(1.0, 0.0, False)
                else:
                    self.want_left, self.want_right = False, False
                    self._rotation = 0
                return
            v_delta = self.velocity - self.cars_in_fov['front'].velocity
            x_delta = self.cars_in_fov['front'].back_left.x - self.front_left.x
            sd = safe_dist(self.velocity * (1 / KMpH), self._aggression_rate, self.model.max_brake_acceleration)
            proximity = x_delta / sd
            if proximity > 2.5 or v_delta < -5:
                self._to_desired_velocity()
            elif proximity < 0.5 or v_delta > 5:
                self._set_acceleration_for_time(1, 0.1, breaking=True)
            elif proximity < 1.5 and v_delta > 1:
                self._set_acceleration_for_time(0.2, 0.1, breaking=True)
            elif proximity < 1:
                self._set_acceleration_for_time(0.1, 0.1, breaking=True)
            else:
               # self._check_lane_change('left')
                #self._check_lane_change('right')
                self._set_acceleration_for_time(0.0, 0.3, breaking=True)


    def make_driving_decision(self) -> None:
        if self.is_broken:
            if self.velocity == 0 and self._reaction_delay <= 0:
                self.want_left, self.want_right = False, False
                self.is_broken = False
            elif self.velocity >= 1:
                self.stop_signals = True
                self._acceleration = Car.ACCIDENT_ACCELERATION
            elif 0 < self.velocity < 1:
                self.want_left, self.want_right, self.stop_signals = True, True, False
                self._turn_signal_time = 0
                self._reaction_delay = Car.REPAIR_TIME
                self._acceleration = 0.0
                self.velocity = 0.0
        elif self._reaction_delay <= 0:
            self._set_reaction()
